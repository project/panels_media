<?php

/**
 * @file
 * Custom content type.
 *
 * This content type is nothing more than a title and a body that is entered
 * by the user and run through standard filters. The information is stored
 * right in the config, so each custom content is unique.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'key' => 'file',
  'single' => TRUE,
  'title' => t('File'),
  'icon' => 'icon_file.png',
  'description' => t('Create a file.'),
  'top level' => TRUE,
  'category' => t('Custom'),
  'defaults' => array(
    'admin_title' => '',
    'title' => '',
    'file' => '',
		'files' => '',
		'file_style' => '',
		'files_preview' => '',
    'file_title' => '',
    'file_description' => '',
    'file_css' => '',
  ),
  // render callback is automatically deduced:
  'render callback' => 'pmfile_file_content_type_render',
  'js' => array('misc/autocomplete.js', 'misc/textarea.js', 'misc/collapse.js'),
  'all contexts' => TRUE,
);

/**
 * Output function for the 'custom' content type. Outputs a custom
 * based on the module and delta supplied in the configuration.
 */
function pmfile_file_content_type_render($subtype, $conf, $args, $contexts) {
  static $delta = 0;

  $block          = new stdClass();
  $block->subtype = ++$delta;
  $block->title   = filter_xss_admin($conf['title']);
  $block->content = theme('pmfile', array('conf' => $conf));

  return $block;
}

/**
 * Callback to provide administrative info. In this case we'll render the
 * content as long as it's not PHP, which is too risky to render here.
 */
function pmfile_file_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title = filter_xss_admin($conf['title']);  
  $block->content = theme('pmfile', array('conf' => $conf));
  
  return $block;
}

/**
 * Callback to provide the administrative title of the custom content.
 */
function pmfile_file_content_type_admin_title($subtype, $conf) {
  $output = t('Custom file');
  $title = !empty($conf['admin_title']) ? $conf['admin_title'] : $conf['title'];
  if ($title) {
    $output = t('Custom file: @title', array('@title' => $title));
  }
  return $output;
}

/**
 * Returns an edit form for the custom type.
 */
function pmfile_file_content_type_edit_form($form, &$form_state) {
	//dsm($form_state);
	//dsm($form_state['conf']);
	// dsm($form);
	
  $conf = $form_state['conf'];
	$form['#attributes'] = array('enctype' => "multipart/form-data");

	unset($form['override_title']);
	unset($form['override_title_text']);
	unset($form['override_title_markup']);
	
  $form['title'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['title'],
    '#title' => t('Title'),
    '#description' => t("This will be used as the Pane title"),
  );
	
	if(isset($conf['files'])) {
		$file = $conf['files'];
		if($conf['file_style'] == "file"){
			$form['files'] = array(
				'#value' => $file,
			);
		}
		else {
			// Need to build the $variables array to pass it to the theme layer
			$variables['path'] = $file->uri;
			$variables['style_name'] = "thumbnail";
			
			$form['files_preview'] = array(
				'#markup' =>  theme("image_style", $variables),
			);
			
			$form['files'] = array(
				'#value' => $file,
			);
		}
	}
  
  $form['file'] = array(
    '#title' => t('Upload file'),
    '#type'  => 'file',
		'#description' => t("Upload a file"),
		'#default_value' => isset($conf['files']) ? $conf['files'] : '',
  );
	
	$styles = array();
	$styles = image_style_options(TRUE);
	$styles['file'] = 'file';
	
	// Provide a select field for choosing an image style to use when displaying
  // the image.
  $form['file_style'] = array(
    '#title' => t('Style'),
    '#type' => 'select',
    '#description' => t('Choose an style to use when displaying this file.'),
    // The image_style_options() function returns an array of all available
    // image styles both the key and the value of the array are the image
    // style's name. The fucntion takes on paramater, a boolean flag
    // signifying wether or not the array should include a <none> option.
    '#options' => $styles,
    '#default_value' => $conf['file_style'],
		'#required' => TRUE,
  );
	
  $form['file_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['file_title']) ? $conf['file_title'] : '',
    '#title' => t('File Title'),
    '#description' => t("This will be the name that replaces the filename. If empty, the filename will be used"),
  );
  
  $form['file_description'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['file_description']) ? $conf['file_description'] : '',
    '#title' => t("File description"),
    '#description' => t("The file description"),
  );
  
  $form['file_css'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['file_css']) ? $conf['file_css'] : '',
    '#title' => t("CSS Class"),
    '#description' => t("The CSS Class to apply to the link"),
  );
	
	$form['admin'] = array(
		'#type' => 'fieldset',
		'#title' => t("Administration settings"),
		'#collapsed' => TRUE,
		'#collapsible' => TRUE,
	);
  $form['admin']['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['admin_title']) ? $conf['admin_title'] : '',
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this pane. If blank, the regular title will be used.'),
  );

  return $form;
}


/**
 * We submit the form and ctools saves the data
 * TODO: Add some validators to the file upload
 */
function pmfile_file_content_type_edit_form_submit(&$form, &$form_state) {
	
	if($form_state['conf']['files'] == NULL) {
		$validators = array();  
		$file = file_save_upload('file', $validators, 'public://files/', FILE_EXISTS_RENAME);
		
		$file->description = $file->filename;
    $file->weight = 0;
    $file->new = TRUE;
		$file->status = FILE_STATUS_PERMANENT;
    
		file_save($file);
		// For some reason, I can't load the $file object into $form_state['conf']['file']
		// so I do it in $form_state['conf']['files']
		$form_state['conf']['files'] = file_load($file->fid);
	}
  
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * If we already have an image, we display it
 */
function ctoolscustomplugins_file_preview($form, &$form_state) {
  if ($form_state['conf']['file']) {
    $file = (object)($form_state['conf']['file']);
    $form['#title'] = t("File Details");
    $form['#value'] = theme_panel_file_preview($file);
    $form['#description'] = t("If you want to remove this file just upload a new one");
  }
  return $form;
}
 